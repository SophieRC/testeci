package org.example;


import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    @org.junit.jupiter.api.Test
    public void add() {
        assertEquals(Calculator.add(2,2),4);
    }

    @org.junit.jupiter.api.Test
    public void sub() {
        assertEquals(Calculator.sub(2,2),0);
    }

    @org.junit.jupiter.api.Test
    public void mult() {
        assertEquals(Calculator.mult(2,2),4);
    }

    @org.junit.jupiter.api.Test
    public void div() {
        assertEquals(Calculator.div(2,2),1);
    }
}